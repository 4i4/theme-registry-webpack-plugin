# Theme Registry Webpack Plugin

Scanning certain directory for templates and autogenerate theme registry file for each found theme.\
For more information please check [@4i4/theme-registry](https://www.npmjs.com/package/@4i4/theme-registry).

## Why this package exists?

When working with multiple themes and templates you need to manually add/remove the templates from your registries.\
This plugin automatically doing that for you and keep everything in check.

## Installing:

Using npm:
```shell
$ npm install @4i4/theme-registry-webpack-plugin
```

Using yarn:
```shell
$ yarn add @4i4/theme-registry-webpack-plugin
```

Using pnpm:
```shell
$ pnpm add @4i4/theme-registry-webpack-plugin
```

## Usage:

**Webpack config**
```javascript
const ThemeRegistryPlugin = require("@4i4/theme-registry-webpack-plugin");

const options = {};

module.exports = {
  plugins: [new ThemeRegistryPlugin(options)]
};
```

**Options**

| **Option**  | **Type**  | **Default value** | **Description**                                                           |
|-------------|-----------|-------------------|---------------------------------------------------------------------------|
| `themesDir` | `string`  | ./src/themes      | The directory that contains all themes                                    |
| `templates` | `string`  | ./templates       | The directory that contains all templates for each theme                  |
| `isNextJS`  | `boolean` | false             | If set to `true` then `next/dynamic` will be used instead of `React.lazy` |

## Requirement:

For each theme you need to create `theme.json` file in the root of the theme with the following properties:

| **Option** | **Type**   | **Required** | **Description**                                                                                       |
|------------|------------|--------------|-------------------------------------------------------------------------------------------------------|
| `parent`   | `string`   | No           | The name of the parent theme.                                                                         |
| `context`  | `string[]` | No           | Array of context to be used. In no context is set, then all templates will be set to the default one. |

## Default folder structure:
    .
    ├── ...
    ├── src
    │   └── themes                  # The directory that contains all themes
    │       ├── theme_1             # Theme 1
    │       │   ├── theme.json      # Theme 1 config file
    │       │   └── templates       # The directory that contains all templates
    │       │       ├── layout      # Context directory
    │       │       ├── icons       # Context directory
    │       │       └── grid        # Context directory
    │       │
    │       └── theme_2             # Theme 2
    │           ├── theme.json      # Theme 2 config file
    │           └── templates       # The directory that contains all templates
    │               └── layout      # Context directory
    └── ...

## Theme inheritance:
if `parent` is set in the `theme.json` the theme registry of the parent will be inherited.

**Example**

Structure:

    .
    ├── themes
    │   ├── theme_1
    │   │   └── theme.json
    │   └── theme_2
    │       └── theme.json

./themes/theme_2/theme.json
```json
{
  "parent": "theme_1"
}
```
That will generate the following registry:
```javascript
import parent from "../theme_1/registry";
const registry = parent.clone();
...
export default registry;
```

## Using the context:
By default, all templates will be assigned to the default context.\
If you want certain context directory to be used then you need to set it in the `theme.json`

**Example**

Structure:

    .
    ├── themes
    │   ├── theme_1
    │   │   ├── theme.json
    │   │   ├── layout
    │   │   │   └── page.tsx
    │   │   ├── grid
    │   │   │   ├── grid.tsx
    │   │   │   └── container.tsx
    │   │   └── icons
    │   │       ├── icon_1.tsx
    │   │       └── icon_2.tsx

./themes/theme_1/theme.json
```json
{
  "context": ["icons"]
}
```
That will generate the following registry:
```javascript
import Registry from "@4i4/registry";
import { lazy } from "react";
const registry = new Registry();

// Layout
registry.set("page", lazy(import("./templates/layout/page")));

// Grid
registry.set("grid", lazy(import("./templates/grid/grid")));
registry.set("container", lazy(import("./templates/grid/container")));

// Icons
registry.set("icon_1", lazy(import("./templates/icons/icon_1")), "icons");
registry.set("icon_2", lazy(import("./templates/icons/icon_2")), "icons");

export default registry;
```