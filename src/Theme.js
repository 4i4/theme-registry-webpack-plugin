const { EOL } = require('os')
const path = require('path')
const { join } = require('path')
const fs = require('fs')
const ThemeNotFoundError = require('./ThemeNotFoundError')

class Theme {
  static skip = /.*(?<!((?<!(?:style|type|mock|test|stories)(?:s)?\.?)(?:ts|js|jsx|tsx)))$/
  themeRoot
  registry
  config
  isNextJS

  constructor (themeRoot, isNextJS = false) {
    this.themeRoot = themeRoot
    this.config = this.loadConfig()
    this.registry = this.findRegistry()
    this.isNextJS = isNextJS
  }

  loadConfig () {
    const pathname = join(this.themeRoot, 'theme.json')
    if (fs.existsSync(pathname)) {
      const content = fs.readFileSync(pathname, 'utf8')
      return JSON.parse(content)
    }
    throw new ThemeNotFoundError(`Theme (${this.themeRoot}) could not be found. Missing theme.json file.`)
  }

  findRegistry () {
    // Search for registry.js or registry.ts file or registry directory.
    const basename = fs.readdirSync(this.themeRoot).find(pathname => pathname.match(/^registry(.(js|ts))?$/))
    // If no registry file or directory was found create new one.
    if (!basename) return this.createRegistryFile()

    let pathname = join(this.themeRoot, basename)
    const isDir = fs.lstatSync(pathname).isDirectory()

    if (isDir) {
      // If the pathname is directory check for index.js or index.ts in it.
      const index = fs.readdirSync(pathname).find(pathname => pathname.match(/^index\.(js|ts)/))
      // Create index file if one is missing.
      if (!index) return this.createRegistryFile(true)
      pathname = join(pathname, index)
    }

    return pathname
  }

  createRegistryFile (dir = false) {
    const lines = this.newContent()
    const path = join(this.themeRoot, dir ? 'registry/index.js' : 'registry.js')

    fs.writeFileSync(path, lines.join(EOL), { encoding: 'utf8' })

    return path
  }

  newContent () {
    const content = []

    if (this.config.parent) {
      const pathname = join(this.themeRoot, '..', this.config.parent)

      if (!fs.existsSync(pathname)) {
        throw new ThemeNotFoundError(`Parent theme ${this.config.parent} was not found`)
      }

      const parent = new Theme(pathname)

      if (this.isNextJS) { content.push('import dynamic from "next/dynamic";') } else { content.push('import { lazy } from "react";') }

      content.push(`import parent from "${path.relative(this.themeRoot, parent.findRegistry())}";`)
      content.push('')
      content.push('const registry = parent.clone();')
    } else {
      content.push('/* eslint-disable prettier/prettier */')
      content.push('import Registry from "@4i4/registry";')

      if (this.isNextJS) { content.push('import dynamic from "next/dynamic";') } else { content.push('import { lazy } from "react";') }

      content.push('')
      content.push('const registry = new Registry();')
    }

    content.push('')
    content.push('export default registry;')

    return content
  }

  updateContent (template, action, content = null) {
    const lines = content || this.readRegistry()
    const entry = this.getTemplateObject(template)
    const context = (this.config.context ?? []).indexOf(entry.context) >= 0

    let line;
    if (this.isNextJS) {
      line = `registry.set("${entry.template}", dynamic(() => import("./${entry.path}"), {})${context ? `, "${entry.context}"` : ''});`
    } else {
      line = `registry.set("${entry.template}", lazy(import("./${entry.path}"))${context ? `, "${entry.context}"` : ''});`
    }
    const index = lines.indexOf(line)

    if (action === 'delete' && index >= 0) {
      lines.splice(index, 1)
    } else if (action === 'add' && index < 0) {
      const contextLine = `// ${entry.context.charAt(0).toUpperCase() + entry.context.slice(1)}`
      const contextIndex = lines.indexOf(contextLine)
      if (contextIndex >= 0) {
        lines.splice(contextIndex + 1, 0, line)
      } else {
        lines.splice(lines.length - 1, 0, contextLine, line, '')
      }
    }

    return lines
  }

  readRegistry () {
    const content = fs.readFileSync(this.registry, 'utf8')
    return content.split(EOL)
  }

  writeRegistry (content) {
    if (!this.sameWithOriginal(content)) { fs.writeFileSync(this.registry, content.join(EOL), { encoding: 'utf8' }) }
  }

  sameWithOriginal (content) {
    const original = this.readRegistry()
    return JSON.stringify(original) === JSON.stringify(content)
  }

  getTemplateName (name) {
    name = name.replace(/\.[^/.]+$/, '')
    name = name.split(/(?=[A-Z])/).join('-')
    name = name.split('.').join('--')
    return name.toLowerCase()
  }

  getTemplateObject (template) {
    const root = /registry\.(js|ts)$/.test(this.registry) ? this.themeRoot : join(this.themeRoot, 'registry')
    const relative = path.relative(root, template)
    const components = relative.split(path.sep)
    const entry = {
      context: 'default',
      template: null,
      path: relative.replace(/(\/index)?\.(js|jsx|ts|tsx)$/, '')
    }

    if (components.length === 2) {
      entry.template = this.getTemplateName(components[1].replace(/(?=.*)(?:\.(js|jsx|ts|tsx))$/, ''))
      return entry
    }

    entry.context = components.splice(0, 2)[1]?.toLowerCase()
    entry.template = components.map((item, index) => {
      const name = this.getTemplateName(item)
      return name === 'index' || name === components[index - 1] ? null : name
    }).filter(Boolean).join('--') || entry.context

    return entry
  }

  addTemplate (template) {
    const lines = this.updateContent(template, 'add')
    this.writeRegistry(lines)
    return this
  }

  deleteTemplate (template) {
    const lines = this.updateContent(template, 'delete')
    this.writeRegistry(lines)
    return this
  }

  rebuild (templatesDir = 'templates') {
    let content = this.newContent()

    try {
      const templates = this.scanTemplatesDir(join(this.themeRoot, templatesDir))
      templates.forEach(template => {
        content = this.updateContent(template, 'add', content)
      })

      this.writeRegistry(content)
    } catch (ex) {
      console.warn(ex)
    }
  }

  scanTemplatesDir (destination) {
    const pathNames = fs.readdirSync(destination)

    let templates = []

    for (const pathname of pathNames) {
      const directory = path.join(destination, pathname)
      const stat = fs.lstatSync(directory)
      if (stat.isDirectory()) {
        templates = [...templates, ...(this.scanTemplatesDir(directory))]
      } else if (stat.isFile() && !Theme.skip.test(pathname)) {
        templates.push(directory)
      }
    }

    return templates
  }
};

module.exports = Theme
