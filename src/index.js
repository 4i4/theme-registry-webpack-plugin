const chokidar = require('chokidar')
const { join } = require('path')
const fs = require('fs')
const Theme = require('./Theme')

class ThemeRegistryWebpackPlugin {
  name = 'ThemeRegistryPlugin'
  watcher

  constructor (options) {
    this.options = {
      themesDir: './src/themes',
      templates: './templates',
      isNextJS: false,
      ...(options ?? {})
    }
  }

  apply (compiler) {
    compiler.hooks.done.tap(this.name, () => {
      if (this.watcher) {
        return
      }
      const pattern = join(this.options.themesDir, '*', this.options.templates, '**/*')
      this.watcher = chokidar.watch(pattern, {
        ignored: (path) => (
          (path !== pattern) &&
          (!fs.existsSync(path) || !fs.lstatSync(path).isDirectory()) &&
          Theme.skip.test(path)
        ),
        ignoreInitial: true
      })
      this.watcher.on('add', path => {
        const theme = this.findByTemplate(path)
        theme?.addTemplate(path)
      })
      this.watcher.on('unlink', path => {
        const theme = this.findByTemplate(path)
        theme?.deleteTemplate(path)
      })
      this.loadAllThemes().forEach(theme => theme.rebuild(this.options.templates))
    })
  }

  findByTemplate (template) {
    const root = join(this.options.themesDir, '.*?)', this.options.templates)
    const regex = new RegExp(`((?:.*/?)${root}.*(?:.js|jsx|ts|tsx)$`)
    const match = template?.match(regex)
    if (match?.[1]) {
      return new Theme(match?.[1], this.options.isNextJS)
    }
    return null
  }

  loadAllThemes () {
    const themes = []
    const directory = this.options.themesDir
    const pathNames = fs.readdirSync(directory)
    for (const pathname of pathNames) {
      const stat = fs.lstatSync(join(directory, pathname))
      if (stat.isDirectory()) {
        try {
          themes.push(new Theme(join(directory, pathname), this.options.isNextJS))
        } catch (ex) {
          console.log(ex.message)
        }
      }
    }
    return themes
  }
}

module.exports = ThemeRegistryWebpackPlugin
