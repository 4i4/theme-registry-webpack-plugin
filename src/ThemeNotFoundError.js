class ThemeNotFoundError extends Error {
  constructor (msg) {
    super(msg)

    // Set the prototype explicitly.
    Object.setPrototypeOf(this, ThemeNotFoundError.prototype)
  }
}

module.exports = ThemeNotFoundError
